const gulp = require('gulp');
const debounce = require('gulp-debounce');
const ts = require('gulp-typescript');
const amdcheck = require('gulp-amdcheck');
const del = require('del');

const tsProject = ts.createProject('tsconfig.json');

gulp.task('default', function() {
    return tsProject.src()
        .pipe(debounce({ wait: 1000 }))
        .pipe(tsProject())
        .pipe(amdcheck())
        .pipe(gulp.dest('nyaa-university'));
});
gulp.task('watch', function() {
    gulp.watch('src/**/*.ts', { ignoreInitial: false }, gulp.series('default'));
});
gulp.task('clean', function() {
    return del('nyaa-university/**/*.js');
});
