module Class {
    export enum FieldIds {
        HEADER_TEXT = 'custpage_nyaa_header_text',
        RESET_BUTTON = 'custpage_nyaa_reset',
        INLINE_CSS = 'custpage_nyaa_inline_css'
    }

    export enum SublistIds {
        CLASS_OFFERING = 'custpage_nyaa_class_sublist'
    }
    
    export enum SublistFieldIds {
        ID = 'custpage_nyaa_class_sublist_id',
        NAME = 'custpage_nyaa_class_sublist_name',
        CLASS_CODE = 'custpage_nyaa_class_sublist_code',
        SLOTS_OFFERED = 'custpage_nyaa_class_sublist_offered',
        SLOTS_OCCUPIED = 'custpage_nyaa_class_sublist_occupied',
        SLOTS_REMAINING = 'custpage_nyaa_class_sublist_remaining'
    }

    export interface ClassOfferingSublistRecord {
        id: number,
        name: string,
        classCode: string,
        slotsOffered: number,
        slotsOccupied: number,
        slotsRemaining: number
    }
}

export = Class;

/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 */
