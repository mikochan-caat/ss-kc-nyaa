import * as log from 'N/log';
import * as record from 'N/record';

module Class {
    export const TYPE = 'customrecord_nyaa_class';

    export enum FieldIds {
        START_DATE = 'custrecord_nyaa_class_start_date',
        SLOTS_OFFERED = 'custrecord_nyaa_class_slots_offered',
        SLOTS_OCCUPIED = 'custrecord_nyaa_class_slots_occupied', 
        SLOTS_REMAINING = 'custrecord_nyaa_class_slots_remaining', 
        STATUS = 'custrecord_nyaa_class_status',
        CLASS_CODE = 'custrecord_nyaa_class_code'
    }

    export enum SublistIds {
        ENROLLEES = 'recmachcustrecord_nyaa_enrollment_class'
    }

    export enum StatusIds {
        OPEN = 1,
        LIMITED = 2,
        FULL = 3,
        CLOSED = 4
    }

    export function isAvailable(classId: record.FieldValue) : boolean {
        const classRecord = record.load({
            type: TYPE,
            id: classId
        });

        // We use these two statistics instead of relying on the Status, just to be sure
        const numberOfSlotsOffered = classRecord.getValue(FieldIds.SLOTS_OFFERED);
        const numberOfSlotsOccupied = classRecord.getValue(FieldIds.SLOTS_OCCUPIED);

        return numberOfSlotsOccupied >= 0 && numberOfSlotsOccupied < numberOfSlotsOffered;
    }

    export function updateStatus(classIdOrRecord: record.FieldValue | record.Record, commitImmediately: boolean = true) : void {
        let classRecord : record.Record;
        if (!Array.isArray(classIdOrRecord) && typeof classIdOrRecord === 'object') {
            classRecord = <record.Record>classIdOrRecord;
        } else {
            classRecord = record.load({
                type: TYPE,
                id: <record.FieldValue>classIdOrRecord
            });
        }

        const numberOfSlotsOffered = classRecord.getValue(FieldIds.SLOTS_OFFERED);
        const numberOfSlotsOccupied = classRecord.getValue(FieldIds.SLOTS_OCCUPIED);
        const limitedThreshold = parseInt(<string>numberOfSlotsOffered, 10) * (2 / 3);

        let newClassStatus;
        if (numberOfSlotsOccupied >= numberOfSlotsOffered) {
            newClassStatus = StatusIds.FULL;
        } else if (numberOfSlotsOccupied >= limitedThreshold) {
            newClassStatus = StatusIds.LIMITED;
        } else {
            newClassStatus = StatusIds.OPEN;
        }

        const oldClassStatus = classRecord.getValue(FieldIds.STATUS);
        log.audit(`Class Offering Status Update - ID: ${classRecord.id}`, 
                  `Max: ${numberOfSlotsOffered}, Thresh: ${limitedThreshold}, Curr: ${numberOfSlotsOccupied}, ` +
                  `Status ${oldClassStatus} -> ${newClassStatus}`);

        classRecord.setValue(FieldIds.STATUS, newClassStatus);
        if (commitImmediately) {
            classRecord.save();
        }
    }
}

export = Class;

/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 */
