module Enrollment {
    export const TYPE = 'customrecord_nyaa_enrollment';

    export enum FieldIds {
        STUDENT_NUMBER = 'custrecord_nyaa_enrollment_student_id',
        DATE_ENROLLED = 'custrecord_nyaa_enrollment_date',
        ENROLLED_CLASS = 'custrecord_nyaa_enrollment_class',
        ENROLLMENT_STATUS = 'custrecord_nyaa_enrollment_status',
        CLASS_STATUS = 'custrecord_nyaa_enrollment_class_status'
    }

    export enum StatusIds {
        PENDING = 1,
        CONFIRMED = 2,
        AUTO_CANCELLED = 3
    }
}

export = Enrollment;

/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 */
