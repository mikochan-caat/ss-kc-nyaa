import { EntryPoints } from 'N/types';
import * as classOffering from 'SuiteScripts/Nyaa-University/modules/class';
import * as enrollment from 'SuiteScripts/Nyaa-University/modules/enrollment';

const saveRecord : EntryPoints.Client.saveRecord = (context) => {
    const enrollmentRecord = context.currentRecord;

    if (!enrollmentRecord.id) {
        const enrolledClassId = enrollmentRecord.getValue(enrollment.FieldIds.ENROLLED_CLASS);
        if (!classOffering.isAvailable(enrolledClassId)) {
            alert('Could not enroll to the selected class, it is already full.\n' +
                  'You may try again if a slot has been made available in the meantime.');
            return false;
        }
    }
    
    return true;
};

export = {
    saveRecord: saveRecord
};

/**
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 */
