interface NLMultiButton {
    onMainButtonClick(obj: any) : void;
}

declare function getNLMultiButtonByName(id: string) : NLMultiButton;
