import { EntryPoints } from 'N/types';
import * as log from 'N/log';
import * as query from 'N/query';
import * as record from 'N/record';
import * as enrollment from 'SuiteScripts/Nyaa-University/modules/enrollment';

const execute : EntryPoints.Scheduled.execute = (_) => {
    const enrollmentQuery = query.create({
        type: enrollment.TYPE
    });
    const classJoin = enrollmentQuery.autoJoin({
        fieldId: enrollment.FieldIds.ENROLLED_CLASS
    });
    enrollmentQuery.condition = enrollmentQuery.createCondition({
        fieldId: enrollment.FieldIds.ENROLLMENT_STATUS,
        operator: query.Operator.EQUAL,
        values: <any>enrollment.StatusIds.PENDING
    });
    enrollmentQuery.columns = [
        enrollmentQuery.createColumn({
            fieldId: 'id'
        }),
        classJoin.createColumn({
            fieldId: 'id'
        })
    ];

    const classIdMap : any[] = [];
    enrollmentQuery.run().results.forEach(enrollmentResult => {
        const enrollmentId = <number>enrollmentResult.values[0];
        const classId = <number>enrollmentResult.values[1];

        classIdMap[classId] = true; // Right-hand side does not matter
        const enrollmentRecord = record.load({
            type: enrollment.TYPE,
            id: enrollmentId
        });
        enrollmentRecord.setValue(enrollment.FieldIds.ENROLLMENT_STATUS, enrollment.StatusIds.CONFIRMED);
        enrollmentRecord.save();
        
        log.audit('Confirmed Enrollment', `Enrollment ID: ${enrollmentId}, Class ID: ${classId}`);
    });

    log.audit('Affected Class Offering IDs', `Summary: ${Object.keys(classIdMap).join(', ')}`);
};

export = {
    execute: execute
};

/**
 * @NApiVersion 2.0
 * @NScriptType ScheduledScript
 */
