import { EntryPoints } from 'N/types';
import * as log from 'N/log';
import * as query from 'N/query';
import * as record from 'N/record';
import * as serverWidget from 'N/ui/serverWidget';
import * as classOffering from 'SuiteScripts/Nyaa-University/modules/class';
import * as enrollment from 'SuiteScripts/Nyaa-University/modules/enrollment';

const beforeLoad : EntryPoints.UserEvent.beforeLoad = (context) => {
    const form = context.form;
    
    switch (context.type) {
        case context.UserEventType.CREATE:
        case context.UserEventType.COPY:
            const fieldsToHide = [
                classOffering.FieldIds.STATUS,
                classOffering.FieldIds.SLOTS_OCCUPIED,
                classOffering.FieldIds.SLOTS_REMAINING
            ];
            fieldsToHide
                .map(fieldId => form.getField({ 
                    id: fieldId 
                }))
                .forEach(field => field.updateDisplayType({
                    displayType: serverWidget.FieldDisplayType.HIDDEN
                }));
            break;
        case context.UserEventType.EDIT:
            const classCodeField = form.getField({
                id: classOffering.FieldIds.CLASS_CODE
            });
            classCodeField.updateDisplayType({
                displayType: serverWidget.FieldDisplayType.INLINE
            });
            break;
    }
};

const beforeSubmit : EntryPoints.UserEvent.beforeSubmit = (context) => {
    const classRecord = context.newRecord;

    switch (context.type) {
        case context.UserEventType.CREATE:
            const classCode = classRecord.getValue(classOffering.FieldIds.CLASS_CODE);
            const classQuery = query.create({
                type: classOffering.TYPE
            });
            classQuery.condition = classQuery.createCondition({
                fieldId: classOffering.FieldIds.CLASS_CODE,
                operator: query.Operator.IS,
                values: <any>classCode
            });
            classQuery.columns = [
                classQuery.createColumn({
                    fieldId: 'id'
                })
            ];

            if (classQuery.run().results.length) {
                throw `Could not create Class Offering, the Class Code '${classCode}' already exists.`;
            }

            classRecord.setValue(classOffering.FieldIds.STATUS, classOffering.StatusIds.OPEN);
            log.audit(`Class Offering Status Update - Code: ${classCode}`, `Initial set to OPEN`);
            break;
        case context.UserEventType.EDIT:
            const oldClassRecord = context.oldRecord;
            const oldNumberOfSlotsOffered = parseInt(<string>oldClassRecord.getValue({
                fieldId: classOffering.FieldIds.SLOTS_OFFERED
            }), 10);
            const newNumberOfSlotsOffered = parseInt(<string>classRecord.getValue({
                fieldId: classOffering.FieldIds.SLOTS_OFFERED
            }), 10);
    
            if (newNumberOfSlotsOffered < oldNumberOfSlotsOffered) {
                log.audit(`Class Offering Resize - ID: ${classRecord.id}`, 
                          `Slots offered decreased to ${newNumberOfSlotsOffered}`);
    
                const enrollmentsQuery = query.create({
                    type: enrollment.TYPE
                });
                const classCondition = enrollmentsQuery.createCondition({
                    fieldId: enrollment.FieldIds.ENROLLED_CLASS,
                    operator: query.Operator.EQUAL,
                    values: <any>classRecord.id
                });
                const statusCondition = enrollmentsQuery.createCondition({
                    fieldId: enrollment.FieldIds.ENROLLMENT_STATUS,
                    operator: query.Operator.EQUAL_NOT,
                    values: <any>enrollment.StatusIds.AUTO_CANCELLED
                });
                const idColumn = enrollmentsQuery.createColumn({
                    fieldId: 'id'
                });
                const dateEnrolledColumn = enrollmentsQuery.createColumn({
                    fieldId: enrollment.FieldIds.DATE_ENROLLED
                });
                enrollmentsQuery.condition = enrollmentsQuery.and(classCondition, statusCondition);
                enrollmentsQuery.columns = [
                    idColumn,
                    dateEnrolledColumn
                ];
                enrollmentsQuery.sort = [
                    enrollmentsQuery.createSort({
                        column: dateEnrolledColumn,
                        ascending: false
                    }),
                    enrollmentsQuery.createSort({
                        column: idColumn,
                        ascending: false
                    })
                ];
    
                const enrollmentResults = enrollmentsQuery.run().results;
                const numberOfSlotsOccupied = parseInt(<string>classRecord.getValue(classOffering.FieldIds.SLOTS_OCCUPIED), 10);
                const slotDifference = Math.max(0, numberOfSlotsOccupied - newNumberOfSlotsOffered);
                for (let i = 0; i < slotDifference; i++) {
                    const result = enrollmentResults[i].values;
                    const resultId = <number>result[0];
                    const resultRecord = record.load({
                        type: enrollment.TYPE,
                        id: resultId
                    });
    
                    resultRecord.setValue(enrollment.FieldIds.ENROLLMENT_STATUS, enrollment.StatusIds.AUTO_CANCELLED);
                    resultRecord.save();
    
                    log.audit(`Class Offering Trim Excess Enrollments - ID: ${classRecord.id}`, 
                              `Auto-cancelled Enrollment ID ${resultId}`);
                }
            } else if (newNumberOfSlotsOffered > oldNumberOfSlotsOffered) {
                log.audit(`Class Offering Resize - ID: ${classRecord.id}`, 
                          `Slots offered increased to ${newNumberOfSlotsOffered}`);
            }
            
            classOffering.updateStatus(classRecord, false);
            break;
    }
};

export = {
    beforeLoad: beforeLoad,
    beforeSubmit: beforeSubmit
};

/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 */
 