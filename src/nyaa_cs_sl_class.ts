import { EntryPoints } from 'N/types';
import * as record from 'N/record';
import * as thisSuitelet from 'SuiteScripts/Nyaa-University/modules/suitelets/class';

const slotsTracker : { [key: string] : SlotInformation } = {};

const lineInit : EntryPoints.Client.lineInit = (context) => {
    const currentRecord = context.currentRecord;

    if (context.sublistId == thisSuitelet.SublistIds.CLASS_OFFERING) {
        const id = <string>currentRecord.getCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.ID
        });
        const numberOfSlotsOffered = parseInt(<string>currentRecord.getCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.SLOTS_OFFERED
        }), 10);
        const numberOfSlotsOccupied = parseInt(<string>currentRecord.getCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.SLOTS_OCCUPIED
        }), 10);
        const numberOfSlotsRemaining = parseInt(<string>currentRecord.getCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.SLOTS_REMAINING
        }), 10);

        if (!slotsTracker[id]) {
            slotsTracker[id] = {
                offered: numberOfSlotsOffered,
                occupied: numberOfSlotsOccupied,
                remaining: numberOfSlotsRemaining
            };
        } else {
            slotsTracker[id].offered = numberOfSlotsOffered;
        }
    }
};

const validateLine : EntryPoints.Client.validateLine = (context) => {
    const currentRecord = context.currentRecord;

    if (context.sublistId == thisSuitelet.SublistIds.CLASS_OFFERING) {
        const id = <string>currentRecord.getCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.ID
        });
        const numberOfSlotsOffered = parseInt(<string>currentRecord.getCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.SLOTS_OFFERED
        }), 10);
        const slotInformation = slotsTracker[id];

        if (numberOfSlotsOffered <= 0) {
            currentRecord.setCurrentSublistValue({
                sublistId: context.sublistId,
                fieldId: thisSuitelet.SublistFieldIds.SLOTS_OFFERED,
                value: slotInformation.offered
            });

            alert('Slots Offered must be at least 1.');
            return false;
        }

        const newRemaining = numberOfSlotsOffered - slotInformation.occupied;

        let newRemainingText : string;
        if (slotInformation.remaining != newRemaining) {
            newRemainingText = `${slotInformation.remaining} -> ${Math.max(0, newRemaining)}`;
        } else {
            newRemainingText = `${slotInformation.remaining}`;
        }
        currentRecord.setCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.SLOTS_REMAINING,
            value: newRemainingText
        });

        let newOccupiedText : string;
        if (newRemaining < 0) {
            newOccupiedText = `${slotInformation.occupied} (${newRemaining})`;
        } else {
            newOccupiedText = `${slotInformation.occupied}`;
        }
        currentRecord.setCurrentSublistValue({
            sublistId: context.sublistId,
            fieldId: thisSuitelet.SublistFieldIds.SLOTS_OCCUPIED,
            value: newOccupiedText
        });
    }

    return true;
};

const saveRecord : EntryPoints.Client.saveRecord = (_) => {
    return confirm('Are you sure you want to save these changes?');
};

const pageReset = () => {
    location.reload();
};

export = {
    lineInit: lineInit,
    validateLine: validateLine,
    saveRecord: saveRecord,
    pageReset: pageReset
};

interface SlotInformation {
    offered: record.FieldValue,
    occupied: number,
    remaining: number
}

/**
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 */
