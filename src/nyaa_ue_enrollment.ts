import { EntryPoints } from 'N/types';
import * as log from 'N/log';
import * as query from 'N/query';
import * as redirect from 'N/redirect';
import * as serverWidget from 'N/ui/serverWidget';
import * as classOffering from 'SuiteScripts/Nyaa-University/modules/class';
import * as enrollment from 'SuiteScripts/Nyaa-University/modules/enrollment';

const beforeLoad : EntryPoints.UserEvent.beforeLoad = (context) => {
    const form = context.form;

    switch (context.type) {
        case context.UserEventType.CREATE:
        case context.UserEventType.COPY:
            const statusField = form.getField({
                id: enrollment.FieldIds.ENROLLMENT_STATUS
            });
            statusField.updateDisplayType({
                displayType: serverWidget.FieldDisplayType.HIDDEN
            });
            break;
        case context.UserEventType.EDIT:
            const fieldsToInline = [
                enrollment.FieldIds.ENROLLED_CLASS,
                enrollment.FieldIds.STUDENT_NUMBER,
                enrollment.FieldIds.DATE_ENROLLED
            ];
            fieldsToInline
                .map(fieldId => form.getField({ 
                    id: fieldId 
                }))
                .forEach(field => field.updateDisplayType({
                    displayType: serverWidget.FieldDisplayType.INLINE
                }));
            break;
    }
};

const beforeSubmit : EntryPoints.UserEvent.beforeSubmit = (context) => {
    const enrollmentRecord = context.newRecord;

    switch (context.type) {
        case context.UserEventType.CREATE:
            const enrolledClassId = enrollmentRecord.getValue(enrollment.FieldIds.ENROLLED_CLASS);
        
            if (!classOffering.isAvailable(enrolledClassId)) {
                throw 'Could not enroll to this class, there are no open slots left.';
            }

            const studentNumber = enrollmentRecord.getValue(enrollment.FieldIds.STUDENT_NUMBER);
            const enrollmentQuery = query.create({
                type: enrollment.TYPE
            });
            const statusCondition = enrollmentQuery.createCondition({
                fieldId: enrollment.FieldIds.ENROLLMENT_STATUS,
                operator: query.Operator.EQUAL_NOT,
                values: <any>enrollment.StatusIds.AUTO_CANCELLED
            });
            const studentNumberCondition = enrollmentQuery.createCondition({
                fieldId: enrollment.FieldIds.STUDENT_NUMBER,
                operator: query.Operator.IS,
                values: <any>studentNumber
            });
            const enrolledClassCondition = enrollmentQuery.createCondition({
                fieldId: enrollment.FieldIds.ENROLLED_CLASS,
                operator: query.Operator.EQUAL,
                values: <any>enrolledClassId
            });
            enrollmentQuery.condition = enrollmentQuery.and(statusCondition, studentNumberCondition, enrolledClassCondition);
            enrollmentQuery.columns = [
                enrollmentQuery.createColumn({
                    fieldId: 'id'
                })
            ];

            if (enrollmentQuery.run().results.length) {
                throw `Could not create Student Enrollment, the Student Number '${studentNumber}' already exists.`;
            }

            enrollmentRecord.setValue(enrollment.FieldIds.ENROLLMENT_STATUS, enrollment.StatusIds.PENDING);
            log.audit(`Student Enrollment Status Update - Student Number: ${studentNumber}`, 
                      `Initial set to PENDING`);
            break;
    }
};

const afterSubmit : EntryPoints.UserEvent.afterSubmit = (context) => {
    let enrollmentRecord = context.newRecord;
    let redirectInEditMode = false;
    
    switch (context.type) {
        case context.UserEventType.DELETE:
            enrollmentRecord = context.oldRecord;
            redirectInEditMode = true;
        case context.UserEventType.CREATE:
            const enrolledClassId = enrollmentRecord.getValue(enrollment.FieldIds.ENROLLED_CLASS);
            classOffering.updateStatus(enrolledClassId);
            redirect.toRecord({
                type: classOffering.TYPE,
                id: <string>enrolledClassId,
                isEditMode: redirectInEditMode
            });
            break;
    }
};

export = {
    beforeLoad: beforeLoad,
    beforeSubmit: beforeSubmit,
    afterSubmit: afterSubmit
};

/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 */
