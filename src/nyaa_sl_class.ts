import { EntryPoints } from 'N/types';
import * as http from 'N/http';
import * as log from 'N/log';
import * as query from 'N/query';
import * as record from 'N/record';
import * as redirect from 'N/redirect';
import * as runtime from 'N/runtime';
import * as serverWidget from 'N/ui/serverWidget';
import * as classOffering from 'SuiteScripts/Nyaa-University/modules/class';
import * as thisSuitelet from 'SuiteScripts/Nyaa-University/modules/suitelets/class';

const onRequest : EntryPoints.Suitelet.onRequest = (context) => {
    const request = context.request;
    const response = context.response;
    const currentScript = runtime.getCurrentScript();

    switch (request.method) {
        case http.Method.GET:
            createForm(response);
            break;
        case http.Method.POST:
            saveChanges(request);
            redirect.toSuitelet({
                scriptId: currentScript.id,
                deploymentId: currentScript.deploymentId
            });
            break;
    }
};

function createForm(response: http.ServerResponse) : void {
    const form = serverWidget.createForm({
        title: 'Class Offering Bulk Editor'
    });

    const inlineCssField = form.addField({
        type: serverWidget.FieldType.INLINEHTML,
        id: thisSuitelet.FieldIds.INLINE_CSS,
        label: 'Inline CSS'
    });
    inlineCssField.defaultValue = `<style type="text/css">
                                     .uir-insert, .uir-remove, .uir-machinebutton-separator,
                                     .listtextnonedit, .listtextnonedit + .uir-machine-button-row { display: none !important; }
                                   </style>`;

    const headerTextField = form.addField({
        type: serverWidget.FieldType.INLINEHTML,
        id: thisSuitelet.FieldIds.HEADER_TEXT,
        label: 'Header Text'
    });
    headerTextField.defaultValue = `<h1 style="font-size: 1.35em;">
                                   You can edit the slots offered for all Class Offerings here. 
                                   Click 'Save Changes' to save all changes at once.</h1>`;

    const classSublist = form.addSublist({
        type: serverWidget.SublistType.INLINEEDITOR,
        id: thisSuitelet.SublistIds.CLASS_OFFERING,
        label: 'Class Offerings'
    });
    const classSublistFields : (serverWidget.AddFieldOptions | serverWidget.UpdateDisplayTypeOptions)[] = [
        {
            type: serverWidget.FieldType.TEXT,
            id: thisSuitelet.SublistFieldIds.ID,
            label: 'Internal ID',
            displayType: serverWidget.FieldDisplayType.HIDDEN
        },
        {
            type: serverWidget.FieldType.TEXT,
            id: thisSuitelet.SublistFieldIds.NAME,
            label: 'Name', 
            displayType: serverWidget.FieldDisplayType.DISABLED
        },
        {
            type: serverWidget.FieldType.TEXT,
            id: thisSuitelet.SublistFieldIds.CLASS_CODE,
            label: 'Class Code',
            displayType: serverWidget.FieldDisplayType.DISABLED
        },
        {
            type: serverWidget.FieldType.INTEGER,
            id: thisSuitelet.SublistFieldIds.SLOTS_OFFERED,
            label: 'Slots Offered',
            displayType: serverWidget.FieldDisplayType.NORMAL
        },
        {
            type: serverWidget.FieldType.TEXT,
            id: thisSuitelet.SublistFieldIds.SLOTS_OCCUPIED,
            label: 'Slots Occupied',
            displayType: serverWidget.FieldDisplayType.DISABLED
        },
        {
            type: serverWidget.FieldType.TEXT,
            id: thisSuitelet.SublistFieldIds.SLOTS_REMAINING,
            label: 'Slots Remaining',
            displayType: serverWidget.FieldDisplayType.DISABLED
        }
    ];
    classSublistFields.forEach(fieldData => {
        const field = classSublist.addField(<serverWidget.AddFieldOptions>fieldData);
        field.updateDisplayType(<serverWidget.UpdateDisplayTypeOptions>fieldData);
    });

    const sublistData = getClassOfferings();
    for (let i = 0; i < sublistData.length; i++) {
        const classData = sublistData[i];
        const idValueMapping : { [key: string] : any } = {};
        idValueMapping[thisSuitelet.SublistFieldIds.ID] = classData.id;
        idValueMapping[thisSuitelet.SublistFieldIds.NAME] = classData.name;
        idValueMapping[thisSuitelet.SublistFieldIds.CLASS_CODE] = classData.classCode;
        idValueMapping[thisSuitelet.SublistFieldIds.SLOTS_OFFERED] = classData.slotsOffered.toString();
        idValueMapping[thisSuitelet.SublistFieldIds.SLOTS_OCCUPIED] = classData.slotsOccupied.toString();
        idValueMapping[thisSuitelet.SublistFieldIds.SLOTS_REMAINING] = classData.slotsRemaining.toString();
        
        Object.keys(idValueMapping).forEach(key => {
            classSublist.setSublistValue({
                id: key,
                value: idValueMapping[key],
                line: i
            });
        });
    }

    form.addSubmitButton({
        label: 'Save Changes'
    });
    form.addButton({
        id: thisSuitelet.FieldIds.RESET_BUTTON,
        label: 'Reset Changes',
        functionName: 'pageReset'
    });
    form.clientScriptModulePath = 'SuiteScripts/Nyaa-University/nyaa_cs_sl_class';

    response.writePage(form);
}

function saveChanges(request: http.ServerRequest) : void {
    const classCount = request.getLineCount({
        group: thisSuitelet.SublistIds.CLASS_OFFERING
    });
    for (let i = 0; i < classCount; i++) {
        const classId = parseInt(request.getSublistValue({
            group: thisSuitelet.SublistIds.CLASS_OFFERING,
            name: thisSuitelet.SublistFieldIds.ID,
            line: i
        }), 10);
        const classRecord = record.load({
            type: classOffering.TYPE,
            id: classId
        });

        const oldSlotsOffered = parseInt(<string>classRecord.getValue(classOffering.FieldIds.SLOTS_OFFERED), 10);
        const newSlotsOffered = parseInt(request.getSublistValue({
            group: thisSuitelet.SublistIds.CLASS_OFFERING,
            name: thisSuitelet.SublistFieldIds.SLOTS_OFFERED,
            line: i
        }), 10);

        if (newSlotsOffered !== oldSlotsOffered) {
            classRecord.setValue(classOffering.FieldIds.SLOTS_OFFERED, newSlotsOffered);
            classRecord.save();
            log.audit(`Bulk Edit - Class Offering ID: ${classId}`, `Slots Offered ${oldSlotsOffered} -> ${newSlotsOffered}`);
        }
    }
}

function getClassOfferings() : thisSuitelet.ClassOfferingSublistRecord[] {
    const classQuery = query.create({
        type: classOffering.TYPE
    });
    const nameColumn = classQuery.createColumn({
        fieldId: 'name'
    });
    classQuery.columns = [
        classQuery.createColumn({
            fieldId: 'id'
        }),
        nameColumn,
        classQuery.createColumn({
            fieldId: classOffering.FieldIds.CLASS_CODE
        }),
        classQuery.createColumn({
            fieldId: classOffering.FieldIds.SLOTS_OFFERED
        })
    ];
    classQuery.sort = [
        classQuery.createSort({
            column: nameColumn
        })
    ];

    return classQuery.run().results
        .map(result => {
            const values = result.values;
            const classRecord = record.load({
                type: classOffering.TYPE,
                id: values[0]
            });

            return <thisSuitelet.ClassOfferingSublistRecord>{
                id: classRecord.id,
                name: values[1],
                classCode: values[2],
                slotsOffered: values[3],
                // We have to retrieve these from the actual record as these are not available
                // when using the Search or Query APIs
                slotsOccupied: parseInt(<string>classRecord.getValue(classOffering.FieldIds.SLOTS_OCCUPIED), 10),
                slotsRemaining: parseInt(<string>classRecord.getValue(classOffering.FieldIds.SLOTS_REMAINING), 10)
            };
        });
}

export = {
    onRequest: onRequest
};

/**
 * @NApiVersion 2.0
 * @NScriptType Suitelet
 */
